import React from "react";
import logo21 from "../assets/images/logo21.png";
import ios from "../assets/images/ios.png";
import chplay from "../assets/images/chplay.png";

export const LoginInstagram = () => {
  return (
    <div className="wrapper">
      <div className="wrapper__img">
        <img className="wrapper__img-icon" src={logo21} />
      </div>
      <div className="wrapper__form">
        <h3>Instagram</h3>
        <div className="wrapper__form--input">
          <input placeholder=" số điện thoại, tên người dùng hoặc email" />
        </div>
        <div className="wrapper__form--input">
          <input placeholder=" mật khẩu" />
        </div>
        <div>
          <button className="wrapper__form--button" disabled>
            Đăng Nhập
          </button>
        </div>
        <div className="wrapper__form--or">
          <div className="wrapper__form--or wrapper__form--left"></div>
          <div className="wrapper__form--or wrapper__form--text">HOẶC</div>
          <div className="wrapper__form--or wrapper__form--right"></div>
        </div>

        <div className="wrapper__login--facebook">
          <span className="wrapper__login--icon-sp">
            <i className="wrapper__login-icon fab fa-facebook-square"></i>
          </span>
          <a href="">
            <span className="wrapper__login--text">
              Đăng nhập bằng Facebook
            </span>
          </a>
        </div>
        <div className="wrapper__forgot">
          <a href="">Quên mật khẩu?</a>
        </div>
        <div className="wrapper__create--account">
          <span>Bạn chưa có tài khoản ư?</span>
          <a href="">Đăng ký</a>
        </div>
        <p className="download-here">Tải ứng dụng.</p>
        <div className="wrapper__download">
          <a href="" className="wrapper__download--link">
            <img src={ios} className="wrapper__download--img" />
          </a>
          <a href="" className="wrapper__download--link">
            <img src={chplay} className="wrapper__download--img" />
          </a>
        </div>
      </div>
    </div>
  );
};

export default LoginInstagram;
