import React from "react";

export const Footer = () => {
  return (
    <div className="wrapper__footer">
      <ul className="wrapper__footer-ul">
        <li>
          <a href="">Meta</a>
        </li>
        <li>
          <a href="">Giới thiệu</a>
        </li>
        <li>
          <a href="">Blog</a>
        </li>
        <li>
          <a href="">Việc làm</a>
        </li>
        <li>
          <a href="">Trợ giúp</a>
        </li>
        <li>
          <a href="">API</a>
        </li>
        <li>
          <a href="">Quyền riêng tư</a>
        </li>
        <li>
          <a href="">Điều khoản</a>
        </li>
        <li>
          <a href="">Tài khoản liên quan nhất</a>
        </li>
        <li>
          <a href="">Hashtag</a>
        </li>
        <li>
          <a href="">Vị trí</a>
        </li>
        <li>
          <a href="">Instagram Lite</a>
        </li>
      </ul>

      <ul className="wrapper__footer--music">
        <li>
          <a href="">Khiêu vũ</a>
        </li>
        <li>
          <a href="">Ẩm thực</a>
        </li>
        <li>
          <a href="">Nhà & vườn</a>
        </li>
        <li>
          <a href="">Âm nhạc</a>
        </li>
        <li>
          <a href="">Nghệ thuật thị giác</a>
        </li>
      </ul>

      <ul className="wrapper__footer--language">
        <li>
          <a href="">Tiếng Việt</a>
          <i className="wrapper__footer--icon fas fa-angle-down"></i>
        </li>
        <li>
          <a href="">© 2022 Instagram from Meta</a>
        </li>
      </ul>
    </div>
  );
};
export default Footer;
