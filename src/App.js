import "./App.css";
import LoginInstagram from "./components/LoginInstagram.js";
import Footer from "./components/Footer.js";

function App() {
  return (
    <div>
      <LoginInstagram />
      <Footer />
    </div>
  );
}

export default App;
